<?php

use Bookstore\Domain\Customer\Basic;
use Bookstore\Domain\Customer\Premium;
use Bookstore\Domain\Customer\CustomerFactory;
use Bookstore\Domain\Customer;
use Bookstore\Domain\Payer;
use Bookstore\Domain\Person;
use Bookstore\Domain\Book;
use Bookstore\Core\Config;
use Bookstore\Utils\Unique;
use Bookstore\Exceptions\InvalidIdException;
use Bookstore\Exceptions\ExceededMaxAllowedException;

// require_once 'vendor/autoload.php';

// $value = 'Petr Kosvanec greets him self';
// setcookie("TestCookie", $value, time()+3600); /* expire in 1 hour */
// echo $_COOKIE["TestCookie"];

$dbConfig = Config::getInstance()->get('db');
$db = new PDO(
  'mysql:host=127.0.0.1;dbname=bookstore',
  $dbConfig['user'],
  $dbConfig['password']

);
$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

function addBook(int $id, int $amount = 1): void {
  $db = new PDO(
    'mysql:host=127.0.0.1;dbnamde=bookstore',
    'root',
    ''
  );

  $query = 'UPDATE book SET stock = stock + :n WHERE id = :id';
  $statement = $db->prepare($query);
  $statement->bindValue('id', $id);
  $statement->bindValue('n', $amount);

  if (!$statement->execute()) {
    throw new Exception($statement->errorInfo()[2]);
  }
}

function addSale(int $userId, array $bookIds): void {
  $db = new PDO(
    'mysql:host=127.0.0.1;dbname=bookstore',
    'root',
    ''
  );

  $db->beginTransaction();
  try {
    $query = 'INSERT INTO sale (customer_id, date) '
        . 'VALUES(:id, NOW())';
    $statement = $db->prepare($query);
    if (!$statement->execute(['id' => $userId])) {
      throw new Exception($statement->errorInfo()[2]);
    }
    $saleId = $db->lastInsertId();

    $query = 'INSERT INTO sale_book (book_id, sale_id) '
        . 'VALUES (:book, :sale)';
    $statement = $db->prepare($query);
    $statement->bindValue('sale', $saleId);
    foreach ($bookIds as $bookId) {
      $statement->bindValue('book', $bookId);
      if (!$statement->execute()) {
        throw new Exception($statement->errorInfo()[2]);
      }
    }

    $db->commit();
  }
  catch (Exception $e) {
    $db->rollBack();
    throw $e;
  }
}

try {
  addSale(2, [1, 1, 1, 2, 2, 3]);
}
catch (Exception $e) {
  echo 'Error adding sale: ' . $e->getMessage();
}
