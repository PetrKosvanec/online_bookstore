<?php

use Bookstore\Core\Config;
use Bookstore\Core\Router;
use Bookstore\Core\Request;
use Bookstore\Models\BookModel;
use Bookstore\Core\Db;

// the FOLLOWING LINE IS NOT FROM LOPEZ, it is from Rayne on stackoverflow.com:
require_once __DIR__ . '/vendor/autoload.php';

$router = new Router();
$response = $router->route(new Request());
echo $response;

$loader = new Twig_Loader_Filesystem(__DIR__ . '/views');
$twig = new Twig_Environment($loader);

// $bookModel = new BookModel(Db::getInstance());
// $book = $bookModel->get(1);
//
// $params = ['book' => $book];
// echo $twig->loadTemplate('book.twig')->render($params);
// $bookModel = new BookModel(Db::getInstance());
// $books = $bookModel->getAll(1, 3);
//
// $params   = ['books' => $books, 'currentPage' => 2];
// echo $twig->loadTemplate('books.twig')->render($params);

$saleModel = new SaleModel(Db::getInstance());
$sale = $saleModel->get(1);

$params = ['sale' => $sale];
echo $twig->loadTemplate('sale.twig')->render($params);
/*
$loader = new Twig_Loader_Filesystem(__DIR__ . '/views');
$twig = new Twig_Environment($loader);


$params = ['book' => $book];
echo $twig->loadTemplate('book.twig')->render($params);


$bookModel = new BookModel(Db::getInstance());
$book = $bookModel->get(1);
$params = ['book' => $book];
echo $twig->loadTemplate('book.twig')->render($params);



$config = new Config();

$dbConfig = $config->get('db');
$db = new PDO(
    'mysql:host=127.0.0.1;dbname=bookstore',
    $dbConfig['user'],
    $dbConfig['password']
);

$loader = new Twig_Loader_Filesystem(__DIR__ . '/views');
$view = new Twig_Environment($loader);
$params = ['book' => $book];
echo $view->loadTemplate('book.twig')->render($params);

$di->set('Utils\Config', $config);
$di->set('Twig_Environment', $view);

*/
