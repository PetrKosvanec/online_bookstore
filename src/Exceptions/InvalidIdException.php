<?php

namespace Bookstore\Exceptions;
use Exception;

class InvalidIdException extends Exception {

  public function __construct($message = null) {

    $message = $message ?: 'Invalid id provided.';
    /* $a = ($fruit ? $fruit : 'apple');
    $a = ($fruit ? : 'apple'); //this will evaluate $fruit only once, and if it evaluates to FALSE, then $a will be set to 'apple'
?>

But remember that a non-empty string '0' evaluates to FALSE!

<?php
$fruit = '1';
$a = ($fruit ? : 'apple'); //this line will set $a to '1'
$fruit = '0';
$a = ($fruit ? : 'apple'); //this line will set $a to 'apple', not '0'!

    */

    parent::__construct($message);

  }

}
